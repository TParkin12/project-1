﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReaction : MonoBehaviour
{
    private float LastKnownHP;


    MeshRenderer mesh;


    TakeDamage Damage;

    private float force = 5;



    // Start is called before the first frame update
    void Start()
    {
        Damage = gameObject.GetComponent<TakeDamage>();
        mesh = gameObject.GetComponent<MeshRenderer>();
        LastKnownHP = Damage.HealthPoints;
    }

    // Update is called once per frame
    void Update()
    {

        if (LastKnownHP > Damage.HealthPoints)
        {
          

            StartCoroutine(Reaction());
        
        }

       

    }

    IEnumerator Reaction()
    {

        mesh.material.color = Color.red;

        LastKnownHP = Damage.HealthPoints;

        yield return new WaitForSeconds(0.2f);

        mesh.material.color = Color.white;

    }


    //public void KnockBack(Vector3 DirectionCamera)
    //{
    //    Vector3 Direction = (transform.position - DirectionCamera);

    //    Direction = Direction.normalized;

    //    gameObject.GetComponent<Rigidbody>().AddForce( Direction * force, ForceMode.Impulse);
    //}

}
