﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallwayDoor : MonoBehaviour
{

    private enum State
    {
        Open,
        Closed
    };

    public GameObject Door1;
    public GameObject Door2;
    private Vector3 Door1Movement;
    private Vector3 Door2Movement;
    private Vector3 DoorClosedPosition;
    private State DoorStatus;
    private float DoorAnimationSpeed;

    void Awake()
    {
        DoorAnimationSpeed = 0.2f;

        

        Door1Movement = new Vector3(-0.2600568f, 0, 0.1f);
        Door2Movement = new Vector3(-0.2600568f, 0, -0.1f);

        DoorClosedPosition = Door1.transform.localPosition;

        DoorStatus = State.Closed;
    }

    private void OnTriggerStay(Collider other)
    {

        switch(DoorStatus)
        {
            case State.Closed:

                if (Input.GetKeyDown(KeyCode.E) && other.CompareTag("Player"))
                {
                    Door1.transform.localPosition =  Vector3.Lerp(Door1Movement, Door1.transform.localPosition, DoorAnimationSpeed);
                    Door2.transform.localPosition =  Vector3.Lerp(Door2Movement, Door2.transform.localPosition, DoorAnimationSpeed);

                    DoorStatus = State.Open;
                }
                break;

            case State.Open:
                if (Input.GetKeyDown(KeyCode.E) && other.CompareTag("Player"))
                {
                    Door1.transform.localPosition = Vector3.Lerp(DoorClosedPosition, Door1.transform.localPosition, DoorAnimationSpeed);
                    Door2.transform.localPosition = Vector3.Lerp(DoorClosedPosition, Door2.transform.localPosition, DoorAnimationSpeed);

                    DoorStatus = State.Closed;
                }
                
                break;
        }

        
        
    }
}
