﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeypadControls : MonoBehaviour
{

    public enum State
    {
        On,
        Off
    };

    private enum MenuState
    {
        open,
        closed
    };

    [Header("Objects to Control")]
    public GameObject[] BedLights;
    public GameObject[] Blinds;
    public GameObject[] RoofBlinds;
    [Header("LocalVariables")]
    [SerializeField]private Canvas KeypadMenu;
    [SerializeField]private Button LightSwitchBtn;
    [SerializeField] private Button BlindsBtn;
    [SerializeField]private Button ChangeColour;
    [SerializeField] private float UseRange;
    private MenuState MState;

    [Header("Controls")]
    public State BlindsState;
    public State SwitchState;
    public Color LightColor;

    void Awake()
    {

        UseRange = 20;
        
        KeypadMenu.enabled = false;

        MState = MenuState.closed;

        foreach (GameObject L in BedLights)
        {
            L.GetComponentInChildren<Light>().color = LightColor;

        }
    }

   
    void Start()
    {
        LightSwitchBtn.onClick.AddListener(LightSwitch);
        BlindsBtn.onClick.AddListener(BlindControl);
    }


    private void Update()
    {

        switch(MState)
        {
            case MenuState.closed:
                if (Input.GetKeyDown(KeyCode.E))
                {
                    CheckHit();
                    
                }
                break;

            case MenuState.open:
                if (Input.GetKeyDown(KeyCode.E))
                {
                    KeypadMenu.enabled = false;
                    Cursor.lockState = CursorLockMode.Locked;
                    Time.timeScale = 1;
                    MState = MenuState.closed;

                }
                break;
        }

       
    }



    void CheckHit()
    {
        RaycastHit Hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray, out Hit, UseRange))
        {
            if(Hit.transform.GetComponent<KeypadControls>() != null)
            {
                KeypadMenu.enabled = true;
                MState = MenuState.open;

                Cursor.lockState = CursorLockMode.None;
                Time.timeScale = 0;



            }
        }

    }


    void LightSwitch()
    {
        switch(SwitchState)
        {
            case State.Off:

                foreach (GameObject L in BedLights)
                {
                    L.GetComponentInChildren<Light>().enabled = true;

                }



                SwitchState = State.On;

                break;

            case State.On:

                foreach (GameObject L in BedLights)
                {
                    L.GetComponentInChildren<Light>().enabled = false;

                }

                SwitchState = State.Off;

                break;
        }

        
    }

    void BlindControl()
    {

        switch(BlindsState)
        {
            case State.Off:

                foreach (GameObject B in Blinds)
                {
                    B.transform.localPosition = new Vector3(B.transform.localPosition.x, 28.71f, B.transform.localPosition.z);
                }

                foreach (GameObject RB in RoofBlinds)
                {
                    RB.transform.localPosition = new Vector3(RB.transform.localPosition.x, 63, RB.transform.localPosition.z);

                }

                BlindsState = State.On;

                break;

            case State.On:

                foreach (GameObject B in Blinds)
                {
                    B.transform.localPosition = new Vector3(B.transform.localPosition.x, 13.00399f, B.transform.localPosition.z);
                }

                foreach (GameObject RB in RoofBlinds)
                {
                    RB.transform.localPosition = new Vector3(RB.transform.localPosition.x, 43, RB.transform.localPosition.z);

                }


                BlindsState = State.Off;
                break;

        }

        
    }

}
