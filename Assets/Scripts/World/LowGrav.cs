﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowGrav : MonoBehaviour
{


    public bool TurnedOn;

    private Vector3 Force;

    // Start is called before the first frame update
    void Start()
    {
        TurnedOn = false;

        Force = new Vector3(0, 0.02f, 0);
    }





    private void OnTriggerStay(Collider other)
    {
        if (TurnedOn == true)
        {
            FindObjectOfType<FPSController>().Gravity = -1;
            FPSController.JumpHeight = 10;
            if (other.tag != "Player")
            {
                other.gameObject.GetComponent<Rigidbody>().useGravity = false;
                other.gameObject.GetComponent<Rigidbody>().AddForce(Force);
                other.gameObject.GetComponent<Rigidbody>().mass = 0.1f;
            }
            

        }
        else
        {
            FindObjectOfType<FPSController>().Gravity = -9.81f;
            FPSController.JumpHeight = 1.5f;
            if (other.tag != "Player")
            {
                other.gameObject.GetComponent<Rigidbody>().useGravity = true;
                other.gameObject.GetComponent<Rigidbody>().AddForce(0, 0, 0);
                other.gameObject.GetComponent<Rigidbody>().mass = 1;
            }
        }
        

    }

    private void OnTriggerExit(Collider other)
    {
       

        if (other.tag != "Player")
        {
            other.gameObject.GetComponent<Rigidbody>().useGravity = true;
            other.gameObject.GetComponent<Rigidbody>().mass = 1f;

        }
        else
        {
            FindObjectOfType<FPSController>().Gravity = -9.81f;
            FPSController.JumpHeight = 1.5f;
        }
    }



   

}
