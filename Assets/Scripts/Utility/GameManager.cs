﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    [Header("States")]
    bool GameEnd = false;

    [Header("Controls")]
    public float GameOverDelay = 2f;



    private void Start()
    {
        Time.timeScale = 1;
    }


    public void GameOver()
    {
        if (GameEnd == false)
        {
            GameEnd = true;
            Invoke("ReloadGame", GameOverDelay);
        }
    }

    void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/SaveGame.gd");

        bf.Serialize(file, SaveData.Current);
        file.Close();
    }

    public static void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/SaveGame.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/SaveGame.gd", FileMode.Open);
            SaveData.Current = (SaveData)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            SaveData.Current = new SaveData();
        }
    }


}
