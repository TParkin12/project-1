﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DevConsole
{
    public abstract class Commands
    {
        public abstract string Name { get; protected set; }

        public abstract string Command { get; protected set; }

        public abstract string Description { get; protected set; }

        public abstract string Help { get; protected set; }


        public void AddCommandConsole()
        {

        }

        public abstract void RunCommand();




    }


    public class DeveloperConsole : MonoBehaviour
    {
        public static DeveloperConsole Instance { get; private set; }
        public static Dictionary<string, Commands> Command { get; private set; }

        //[Header("UI Components")]
        public Canvas console;
        public ScrollRect scrollRect;
        public Text consoleText;
        public Text Input;
        public InputField ConsoleInput;



        private void Awake()
        {
            if (Instance != null)
            {
                return;
            }
            Instance = this;
            Command = new Dictionary<string, Commands>();
        }
        
    }
}