﻿using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public static SaveData Current;
    public float HighScore;
}