﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Controls : MonoBehaviour
{

    public Text Demo1;
    public Text Demo2;

    private bool flip;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            
            if (flip == false)
            {
                Demo1.enabled = false;
                Demo2.enabled = true;

                flip = true;

            }
            else
            {
                Demo1.enabled = true;
                Demo2.enabled = false;

                flip = false;
            }

            

        }
    }
}
