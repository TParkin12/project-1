﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VendingMachine : MonoBehaviour
{

    public GameObject Button;
    public GameObject Button2;
    public GameObject Button3;

    public BuffCreator Buff_One;
    public BuffCreator Buff_Two;
    public BuffCreator Buff_Three;


    private BuffCreator BuffManager;

    public GameObject Target;

    // Start is called before the first frame update
    void Start()
    {
        Button.SetActive(false);
        Button2.SetActive(false);
        Button3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Button.SetActive(false);
            Button2.SetActive(false);
            Button3.SetActive(false);
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.F))
        {
            Button.SetActive(true);
            Button2.SetActive(true);
            Button3.SetActive(true);
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;

           
        }




        Button.GetComponent<Button>().onClick.AddListener(Button1Click);

        Button2.GetComponent<Button>().onClick.AddListener(Button2Click);

        Button3.GetComponent<Button>().onClick.AddListener(Button3Click);


        
    }

    private void OnTriggerExit(Collider other)
    {
        Button.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }




    private void Button1Click()
    {
        BuffManager = Buff_One;
        BuffEdit();
        AddEffect();
    }
    private void Button2Click()
    {
        BuffManager = Buff_Two;
        BuffEdit();
        AddEffect();
    }


    private void Button3Click()
    {
        BuffManager = Buff_Three;
        BuffEdit();
        AddEffect();
    }



    private void AddEffect()
    {
         
         
         
         

         

        FPSController Player;

       

         Player = Target.GetComponent<FPSController>();
 

         Player.StatusManager();

        //Player.AddEffect(new Buff(Player));
    }

    private void BuffEdit()
    {
        Buff.ChangeAmount = BuffManager.Multiplier;
       // Buff.type = BuffManager.ApplyTo;
        Buff.Duration = BuffManager.Time;

    }
        
        
     
    }



