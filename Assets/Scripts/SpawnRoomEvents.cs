﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRoomEvents : MonoBehaviour
{
    [Header("References")]
    private Animator Blinds;
    private Animator Lights;
    


    private void Start()
    {
        Blinds = gameObject.GetComponent<Animator>();
        if (GameObject.Find("AnimatedLights") != null)
        {
            Lights = GameObject.Find("AnimatedLights").GetComponent<Animator>();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            Blinds.SetTrigger("OpenBlinds");

            Lights.SetTrigger("LightsOn");

             

        }
    }
}
