﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour
{

    [Header("References")]
    public Transform Camera; // This is the player Body aka Controller.
    public CharacterController Controller; // This is the player Controller
    public LayerMask Floor; //This finds the floor
    public Transform GroundChecker; //Bottom of player model
 

    [Header("Controls")]
    public float MoveSpeed ;
    public float Gravity = -9.81f;
    public float MouseSensitivity = 100f;
    [SerializeField] public static float JumpHeight ;
    [SerializeField] public static float RunMultiplier;

    [Header("Local Variables")] 
    private Vector3 P_Move;
    private float XRot = 0f;
    [HideInInspector] private float FloorHeight = 0.4f; //Supposed to set how high the check for floor is
    [HideInInspector] private Vector3 Velocity; //Speed in a direction
    [HideInInspector] private bool OnGround; //Bool to see if the player is on the ground
    private List<Buff> StatusEffects = new List<Buff>();

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; // this locks the mouse cursor when playing. 
        MoveSpeed = 12f;

        RunMultiplier = MoveSpeed;

        JumpHeight = 1.5f;




    }

    // Update is called once per frame
    void Update()
    {

        MouseLook();

        PlayerMovement();

        PlayerJump();

        PlayerCrouch();

        PlayerSprint();

       
    }

    void MouseLook() //controls the aiming on both axis
    {
        float M_X = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
        float M_Y = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;

        XRot -= M_Y;
        XRot = Mathf.Clamp(XRot, -90, 90);


        Camera.localRotation = Quaternion.Euler(XRot, 0f, 0f);
         
        transform.Rotate(Vector3.up * M_X);
    }

    void PlayerMovement() //player movement Default WASD
    {

        

        float XMove = Input.GetAxis("Horizontal");
        float ZMove = Input.GetAxis("Vertical");

        P_Move = transform.right * XMove + transform.forward * ZMove; // lets the player move from on any axis 

        Controller.Move(P_Move * MoveSpeed * Time.deltaTime); //moves the player in a direction

        
       
    }

    void PlayerJump()
    {

        GroundCheck();

        if (Input.GetButtonDown("Jump") && OnGround == true)
        {
            FindObjectOfType<AudioManager>().Play("Huh");

            Velocity.y = Mathf.Sqrt(JumpHeight * -2f * Gravity); //basic gravity equation for jumps and falls
           
        }

       

        
            Velocity.y += Gravity * Time.deltaTime; //again Gravity Equation, Vy = Gravity * Time^(2), Gravity = -9.81 on earth

            Controller.Move(Velocity * Time.deltaTime); //Squares time and moves the player towards the ground, applies gravity.
        
        
    }



    void PlayerCrouch()
    {

        if (Input.GetKeyDown("c"))
        {
             
            Controller.height = 1.5f;
            //Camera.transform.localScale = CrouchHeight;
            Camera.transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
        }
        else if(Input.GetKeyUp("c"))
        {
            Controller.height = 2f;
            //Camera.transform.localScale = PlayerHeight;
            Camera.transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
        }

    }

    void PlayerSprint() //Replace this placeholder code.
    {

       
        

        if (Input.GetKeyDown(KeyCode.LeftShift) && OnGround)
        {

            MoveSpeed = RunMultiplier * 1.5f;

        }
        if (Input.GetKeyUp(KeyCode.LeftShift) && OnGround)
        {

            MoveSpeed = RunMultiplier / 1.5f;
            MoveSpeed = RunMultiplier;
        }
        //Camera.localPosition = CameraDefault;
         
           
        
       
 
    }

    




    bool GroundCheck() // Check to see if the player is on the ground, then returns true or false
    {
        OnGround = Physics.CheckSphere(GroundChecker.position, FloorHeight, Floor);

       

         
        if (OnGround && Velocity.y < 0)
        {
            Velocity.y = -2f;
        }

        return (OnGround);
    }

 


    
    public void AddEffect(Buff Status)
    {

       

    }


    public void StatusManager()
    {

        Buff.active = true;
       
        
    }
        




}

