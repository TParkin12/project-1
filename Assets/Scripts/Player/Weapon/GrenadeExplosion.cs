﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeExplosion : MonoBehaviour
{

    [Header("References")]
    public GameObject Ex_VFX;

    [Header("Controls")]
    public float Ex_Radius;
    public float Ex_Force;
    public float Ex_Damage;
    public float Up_Modifer;
    public float Fuze;
    [Header("LocalVariables")]
    private Collider[] Col_Hit;


    public void OnCollisionEnter(Collision collisions)
    {
        StartCoroutine(GrenadeLocal(collisions.contacts[0].point));
    }


  


    IEnumerator GrenadeLocal(Vector3 Location)
    {

        yield return new WaitForSeconds(Fuze);

        Col_Hit = Physics.OverlapSphere(Location, Ex_Radius);

        GameObject ExplosionVFX = Instantiate(Ex_VFX, transform.TransformPoint(0, 0, 0), transform.rotation);

        Destroy(ExplosionVFX, 2.4f);

        foreach (Collider hitcol in Col_Hit)
        {
            if (hitcol.GetComponent<TakeDamage>() != null)
            {
                hitcol.GetComponent<TakeDamage>().DamageTaken(Ex_Damage);
            }

            if (hitcol.GetComponent<PlayerStats>() != null)
            {
                hitcol.GetComponent<PlayerStats>().DamageTaken(Ex_Damage);
            }

            if (hitcol.GetComponent<Rigidbody>() != null)
            {
                hitcol.GetComponent<Rigidbody>().isKinematic = false;
                hitcol.GetComponent<Rigidbody>().AddExplosionForce(Ex_Force, Location, Ex_Radius, Up_Modifer, ForceMode.Impulse);

            }

            Destroy(gameObject);
        }

       

       
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Ex_Radius);
    }


}


