﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
public class BasicProjectileWeapon : MonoBehaviour
{

    [Header("Controls")]
    public int Max_Ammo;
    public float ReloadTime;
    public float ShotDelay;
    public float Range = 100;
    public float Damage = 10;


    [Header("References")]
    public Camera PlayerCam;
    public ParticleSystem Muzzle;
    public GameObject HitDecal;
    public AnimationClip ReloadAnimation;
    public AnimationClip FireingAnimation;
    public Animator anim;
    public Transform Laser;
    public LayerMask Enemy;

    [Header("Script/objects")]
    private LineRenderer Laserpointer;
    private Vector3 RayOrigin;

    [Header("Script/LocalVariables")]
    private float FireRate;
    private float NextFire;
    private float CurrentAmmo;
    private bool isReload = false;
    private bool isFireing = false;
    [SerializeField]
    private float KnockBackForce;

    [Header("Debug")]
    public bool Debugger;

    void Start()
    {

        
        
        if (CurrentAmmo == 0)
        {
            CurrentAmmo = Max_Ammo;
        }
       
        Debugger = false;

        FindObjectOfType<AudioManager>().Play("Music");

        Laserpointer = GetComponent<LineRenderer>();

        

    }





    void FixedUpdate()
    {
       

        if (isReload == true)
        {
            return;
        }
        if (CurrentAmmo == 0 || Input.GetKeyDown(KeyCode.R))
        {

            StartCoroutine(Reload());


            return; //code will stop here, and not continue

        }

        if (Input.GetButton("Fire1") && Time.time > NextFire)
        {

            NextFire = Time.time + FireRate;
            RayOrigin = PlayerCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
            Laserpointer.SetPosition(0, Laser.position);

            if (isFireing == true)
            {

                

                return;
            }
            if (isFireing == false && isReload == false)
            {
                
                Fire();
                StartCoroutine(Fireing());

                

                return; //code will stop here, and not continue

            }
        }

        if (Debugger == true)
        {
            Debugging();
        }

    }

    IEnumerator Fireing()
    {
        

        Muzzle.Play();

        Laserpointer.enabled = true;
        

        isFireing = true;


        anim.SetBool("isFireing", true);


        anim.SetFloat("FireRate", FireingAnimation.length / ShotDelay );

        


        FindObjectOfType<AudioManager>().Play("Shotgun");

        yield return new WaitForSeconds(ShotDelay);

        
        FindObjectOfType<AudioManager>().Stop("Shotgun");

        anim.SetBool("isFireing", false);

        

        Laserpointer.enabled = false;

        isFireing = false;

        
       

    }



    IEnumerator Reload()
    {
        isReload = true;

        if (CurrentAmmo == 0)// simple check to see if the last bullet was fired... this means that the reload time should be a tiny bit longer to account for fire animation to finish
        {
            yield return new WaitForSeconds(ShotDelay);
        }


        anim.SetBool("Reloading", true);

        anim.SetFloat("Speed", (ReloadAnimation.length / ReloadTime));

        yield return new WaitForSeconds(ReloadTime);

        anim.SetBool("Reloading", false);

        isReload = false;

        CurrentAmmo = Max_Ammo;

    }





    void Fire()
    {

        
        CurrentAmmo--;





        if (Physics.Raycast(RayOrigin, PlayerCam.transform.forward, out RaycastHit Hit, Range, Enemy))
        {



            TakeDamage GiveDamage = Hit.transform.GetComponent<TakeDamage>();


            Laserpointer.SetPosition(1, Hit.point);


            if (GiveDamage != null)
            {
                GiveDamage.DamageTaken(Damage);

            }

            if (Hit.rigidbody != null)
            {
                Hit.rigidbody.AddForce(-Hit.normal * KnockBackForce);
            }

            GameObject BulletDecal = Instantiate(HitDecal, Hit.point, Quaternion.LookRotation(Hit.normal));
            Destroy(BulletDecal, 3);
        }
        else
        {
            Laserpointer.SetPosition(1, RayOrigin + (PlayerCam.transform.forward * Range));
        }

    }


    void Debugging()
    {
        
        Debug.DrawRay(RayOrigin,PlayerCam.transform.forward * Range, Color.red);
    }




}
