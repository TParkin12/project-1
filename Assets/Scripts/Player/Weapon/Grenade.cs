﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    [Header("References")]
    public GameObject Grenade_Prefab;

    [Header("Controls")]
    public float ThrowSpeed;
    public float GrenadeAmount;

    [Header("LocalVariables")]
    private Transform M_Pos;
    void Start()
    {
        M_Pos = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.G) && GrenadeAmount > 0)
        {
            ThrowGrenade();
        }
        
    }

    void ThrowGrenade()
    {
        GameObject Projectile = Instantiate(Grenade_Prefab, M_Pos.transform.TransformPoint(0, 0, 1.5f), M_Pos.rotation);
        Projectile.GetComponent<Rigidbody>().AddForce(M_Pos.forward * ThrowSpeed, ForceMode.Impulse);
        Destroy(Projectile, 4);
        GrenadeAmount--;
    }
}
