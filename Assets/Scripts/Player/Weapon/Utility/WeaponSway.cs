﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSway : MonoBehaviour
{
    public float SwayAmount;
    public float MaxAmount;
    public float Smoothing;

    private Vector3 intitial;
    void Start()
    {
        intitial = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        float X = Input.GetAxis("Mouse X") * SwayAmount;
        float Y = Input.GetAxis("Mouse Y") * SwayAmount;

        X = Mathf.Clamp(X, -MaxAmount, MaxAmount);
        Y = Mathf.Clamp(Y, -MaxAmount, MaxAmount);

        Vector3 FinalPos = new Vector3(X, Y, 0);
        transform.localPosition = Vector3.Lerp(transform.localPosition, FinalPos + intitial, Time.deltaTime * Smoothing);




    }
}
