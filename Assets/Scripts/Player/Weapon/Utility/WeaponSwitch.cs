﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitch : MonoBehaviour{ 
    
    //Code provided by Brackey's on Youtube link (https://youtu.be/Dn_BUIVdAPg)
    //Will switch out in previous interation using as placeholder/example

    private int CurrentWeapon;



  
    void Start()
    {
        SwitchWeapon();
    }

 
    void Update()
    {

        int PreviousWeapon = CurrentWeapon;

        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (CurrentWeapon >= transform.childCount - 1)
            {
                CurrentWeapon = 0;
            }

            else
            {
                CurrentWeapon++;
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (CurrentWeapon <= 0)
            {
                CurrentWeapon = transform.childCount - 1;
            }

            else
            {
                CurrentWeapon--;
            }
        }

        if(PreviousWeapon != CurrentWeapon)
        {
            SwitchWeapon();
        }
    }

    void SwitchWeapon()
    {
        int i = 0;

        foreach (Transform weapon in transform)
        {
            if (i == CurrentWeapon)
            {
                weapon.gameObject.SetActive(true);
            }
            else
            {
                weapon.gameObject.SetActive(false);
            }

            i++;
        }
    }

}
