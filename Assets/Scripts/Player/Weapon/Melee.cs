﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour
{
    [Header("Controls")]
    public float AttackSpeed;
    public float Delay;
    public float AttackDamage;
    public float AttackRange;
    public float KnockBackForce;

    [Header("References")]
    public Animator anim;
    public Collider Collider;
    public Transform Pos;
    public LayerMask EnemiesLayer;

    [Header("Script/Variables")]
    private float AttackCooldown = 0f;

    void Start()
    {
        Collider.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        

        



        if (Input.GetButton("Fire1") && (Time.time > AttackCooldown) && !anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            Vector3 temp = new Vector3(AttackRange, AttackRange, AttackRange);
            Collider[] EnemyDamage = Physics.OverlapCapsule(Pos.position, temp, AttackRange, EnemiesLayer);
            for (int i = 0; i < EnemyDamage.Length; i++)
            {


                if (EnemyDamage[i].GetComponent<TakeDamage>() != null)
                {
                    EnemyDamage[i].GetComponent<TakeDamage>().DamageTaken(AttackDamage);
                }


                if (EnemyDamage[i].GetComponent<Rigidbody>() != null)
                {
                    EnemyDamage[i].GetComponent<Rigidbody>().AddForce(temp * KnockBackForce);

                }
                
            }

            Collider.enabled = true;

            AttackCooldown = Time.time + Delay;

            Attack();
        }


    }


    
     
    void Attack()
    {
       
            StartCoroutine(DealDamage());
    }


    IEnumerator DealDamage()
    {

        
        anim.SetFloat("AttackSpeed", anim.GetCurrentAnimatorStateInfo(0).length / AttackSpeed);

        anim.SetTrigger("Attack");

        yield return new WaitForSeconds(Delay);

        if ( anim.GetCurrentAnimatorStateInfo(0).length > anim.GetCurrentAnimatorStateInfo(0).normalizedTime )
        {
            anim.ResetTrigger("Attack");

            Collider.enabled = false;

        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(Pos.position, AttackRange);
    }



}
