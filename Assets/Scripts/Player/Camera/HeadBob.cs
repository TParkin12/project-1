﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadBob : MonoBehaviour
{
    private Vector3 NewPos;
    private float timer;
    private float midpoint = 2.0f;

    public float BobbingSpeed;
    public float BobbingAmount;
    public bool HeadBob_Enabled;
    public Camera MainCam;

    private void Update()
    {
        if(!HeadBob_Enabled)
        {
            return;
        }
        else
        {
            float waveslice = 0.0f;
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            if (Mathf.Abs(x) == 0 && Mathf.Abs(y) == 0)
            {
                timer = 0.0f;
            }
            else
            {
                timer = timer + BobbingSpeed;
                if(timer > Mathf.PI * 2)
                {
                    timer = timer - (Mathf.PI * 2);
                }
            
            }

            if (waveslice != 0)
            {
                float change = waveslice * BobbingAmount;
                float totalAxes = Mathf.Abs(x) + Mathf.Abs(y);
                totalAxes = Mathf.Clamp(totalAxes, 0.0f, 1.0f);
                change = totalAxes * change;
                MainCam.transform.localPosition = new Vector3(MainCam.transform.localPosition.x, midpoint * change, MainCam.transform.localPosition.z);
            }
            else
            {
                MainCam.transform.localPosition = new Vector3(MainCam.transform.localPosition.x, midpoint, MainCam.transform.localPosition.z);
            }
           

        }
    }

}
