﻿using UnityEngine;
using UnityEngine.UI;
public class PlayerStats : MonoBehaviour
{
    [Header("Controls")]
    public float HealthPoints;

    [Header("LocalVariables")]
    private float Clamp;
     


 

    public void DamageTaken(float R_Damage)
    {


        float currentHealth = HealthPoints;

        HealthPoints -= R_Damage;

        Clamp = 0.75f - (HealthPoints / currentHealth);
        Clamp = Mathf.Clamp(Clamp, 0, 0.75f);

       




        if (HealthPoints <= 0)
        {
            DeathAnimation();
        }
    }
	

    void Update()
	{
		if(GameObject.Find("HealthBar") != null)
        {
            GameObject.Find("HealthBar").GetComponent<Image>().fillAmount =  1  * (HealthPoints / 100);
            if (Input.GetKeyDown(KeyCode.N))
            {
                HealthPoints -= 10;
            }
            
        }
	}
 

    void DeathAnimation()
    {

        FindObjectOfType<GameManager>().GameOver();

    }

}

