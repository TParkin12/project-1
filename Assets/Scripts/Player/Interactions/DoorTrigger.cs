﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class DoorTrigger : MonoBehaviour
{

    private Animator anim;

    public float CloseDelay;

    public AnimationClip Clip;

    private void Start()
    {
        anim = gameObject.GetComponent<Animator>();

        anim.SetBool("Play", false);

      

    }

    private void OnTriggerEnter(Collider other)
    {

        anim.SetFloat("Direction", 0.2f);
        anim.SetBool("Play", true);
        FindObjectOfType<AudioManager>().Play("DoorOpen");


    }

    private void OnTriggerExit(Collider other)
    {
        anim.SetFloat("Direction", -0.2f);

        StartCoroutine(Timer());

    }

    IEnumerator Timer()
    {
        anim.SetBool("Play", true);

        
        FindObjectOfType<AudioManager>().Play("DoorClose");

        yield return new WaitForSeconds(Clip.length + CloseDelay);

       

        anim.SetBool("Play", false);
    }

}
