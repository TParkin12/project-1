﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.F))
        {
            if(gameObject.GetComponentInParent<LowGrav>().TurnedOn == false)
            {
                gameObject.GetComponentInChildren<Light>().color = Color.blue;
                gameObject.GetComponentInParent<LowGrav>().TurnedOn = true;
            }
            else
            {
                gameObject.GetComponentInChildren<Light>().color = Color.green;
                gameObject.GetComponentInParent<LowGrav>().TurnedOn = false;

            }
        }
         
        
    }

}
