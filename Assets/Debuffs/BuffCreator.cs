﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "Buff", menuName = "Buff/CreateBuff", order = 1) ]
public class BuffCreator : ScriptableObject
{
 
    public float Multiplier;

    public float Time;

    public enum Type
    {
        Buff,
        Debuff
    };

    public enum ThingToEffect
    {
        Health,
        Stanima,
        JumpHeight,
        RunSpeed
    };

    public Type type;

    public ThingToEffect ApplyTo;

 
       
        
       




    }

