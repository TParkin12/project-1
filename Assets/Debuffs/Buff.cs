﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Type
{
    Health,
    Stanima,
    JumpHeight,
    RunSpeed
};

 


public class Buff
{

    public string StatusName;

    public static float ChangeAmount;

    public static float Duration;

    public static Type type;

    public static float delay = 1000;

    protected FPSController Player;


    public static bool active = false;



    public Buff(FPSController Player1)
    {
        Player = Player1;
    }




    public void ActivateBuff()
    {
        if (active == true)
        {
            Duration += Time.deltaTime;

            Debug.Log(Duration);

            if (Duration >= delay)
            {
                Duration -= delay;
            }
            else
            {
                switch (type)
                {
                    case Type.Health:
                        if (Player.GetComponent<PlayerStats>().HealthPoints < 100)
                            Player.GetComponent<PlayerStats>().DamageTaken(ChangeAmount);
                        else
                            ChangeAmount = 0;

                        break;

                    case Type.Stanima:
                        Player.GetComponent<PlayerStats>().DamageTaken(ChangeAmount);

                        break;

                    case Type.JumpHeight:

                        if (FPSController.JumpHeight <= ChangeAmount)
                        {
                            FPSController.JumpHeight += ChangeAmount;
                            Debug.Log(FPSController.JumpHeight);
                        }

                        break;

                    case Type.RunSpeed:



                        if (FPSController.RunMultiplier <= ChangeAmount)
                        {
                            FPSController.RunMultiplier += ChangeAmount;

                            Debug.Log(FPSController.RunMultiplier);
                        }


                        break;


                    default:
                        Player.GetComponent<PlayerStats>().DamageTaken(ChangeAmount);

                        break;
                }

            }
        }
    }

    
}
